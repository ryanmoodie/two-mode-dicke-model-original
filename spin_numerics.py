#!/usr/bin/env python
# coding=utf-8

"""
spin_numerics.py
Module to use SymPy to solve quartic spin_z equations.
"""

from time import clock
from numpy import roots
from sympy import symbols, simplify, Poly, I, sqrt, Eq, solve
from sympy.functions import exp, conjugate

from auxiliary import write_out_raw, plog, LINE, stopwatch, INDENT
from configurer import program_configuration

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "2 Mar 2017"


def sra_spin_z_coefficients(s):
    omega_0, wat, wbt, omega_a, omega_b, u_a, u_b, spin_z, kap, kbp, kappa_a, \
    kappa_b, g, spin_total = \
        symbols('omega_0, omega_a_tilde, omega_b_tilde, omega_a, omega_b, '
                'u_a, u_b, spin_z_0, kappa_a_prime, kappa_b_prime, kappa_a, '
                'kappa_b, g, spin_total')

    wat = omega_a + u_a * spin_z
    wbt = omega_b + u_b * spin_z
    kap = kappa_a / 2
    kbp = kappa_b / 2

    equation = Poly(
        omega_0 * (wat ** 2 + kap ** 2) * (wbt ** 2 + kbp ** 2)
        + 4 * g ** 2 * (spin_total ** 2 - spin_z ** 2) * (
            u_a * wbt ** 2 + u_b * wat ** 2 + u_a * kbp ** 2 + u_b * kap ** 2)
        + 8 * spin_z * g ** 2 * (
            wat * wbt * (wat + wbt) + wbt * kap ** 2 + wat * kbp ** 2), spin_z)

    coefficient_list = equation.all_coeffs()

    analytical_coefficients = [
        simplify(coefficient) for coefficient in coefficient_list]

    plog(s, f"Analytical coefficients:\n{analytical_coefficients}", 2)

    return analytical_coefficients


def srb_spin_z_coefficients(s):
    omega_a, omega_b, u_a, u_b, kap, kbp, kappa_a, kappa_b = \
        symbols('omega_a, omega_b, u_a, u_b, kappa_a_prime, kappa_b_prime, '
                'kappa_a, kappa_b')

    kap = kappa_a / 2
    kbp = kappa_b / 2

    analytical_coefficients = [
        u_a * u_b * (u_a + u_b),
        u_b * omega_a * (u_b + 2 * u_a) + omega_b * u_a * (u_a + 2 * u_b),
        u_a * (omega_b * (omega_b + 2 * omega_a) + kbp ** 2)
        + u_b * (omega_a * (omega_a + 2 * omega_b) + kap ** 2),
        omega_a * (omega_b ** 2 + kbp ** 2)
        + omega_b * (omega_a ** 2 + kap ** 2)
    ]

    plog(s, f"Analytical coefficients:\n{analytical_coefficients}", 2)

    return analytical_coefficients


def zero_g_prime_spin_z_coefficients(s):
    omega_0, wat, wbt, watt, wbtt, omega_a, omega_b, u_a, \
    u_b, spin_z, kap, kbp, kappa_a, kappa_b, g, mu, spin_total = \
        symbols('omega_0, omega_a_tilde, omega_b_tilde, omega_a_tilde_tilde, '
                'omega_b_tilde_tilde, omega_a, omega_b, u_a, u_b, spin_z_0, '
                'kappa_a_prime, kappa_b_prime, kappa_a, kappa_b, g, mu,'
                'spin_total')

    wat = omega_a + u_a * spin_z
    wbt = omega_b + u_b * spin_z
    mu = (wat - wbt) / 2
    watt = wat - mu
    wbtt = wbt + mu
    kap = kappa_a / 2
    kbp = kappa_b / 2

    equation = Poly(
        ((omega_0 - mu) * (watt ** 2 + kap ** 2) * (wbtt ** 2 + kbp ** 2) /
         (g ** 2))
        + 2 * ((watt - I * kap) * (wbtt ** 2 + kbp ** 2)
               + (wbtt + I * kbp) * (watt ** 2 + kap ** 2)) * spin_z
        + ((spin_total ** 2) - (spin_z ** 2)) * (
            u_a * (wbtt ** 2 + kbp ** 2) + u_b * (watt ** 2 + kap ** 2)),
        spin_z)

    coefficient_list = equation.all_coeffs()

    analytical_coefficients = [
        simplify(coefficient) for coefficient in coefficient_list]

    plog(s, f"Analytical coefficients:\n{analytical_coefficients}", 2)

    return analytical_coefficients


# def i_gamma_spin_z_coefficients(s):
#     omega_0, wat, wbt, watt, wbtt, omega_a, omega_b, u_a, \
#     u_b, spin_z, kap, kbp, kappa_a, kappa_b, g, mu, spin_total = \
#         symbols('omega_0, omega_a_tilde, omega_b_tilde, omega_a_tilde_tilde, '
#                 'omega_b_tilde_tilde, omega_a, omega_b, u_a, u_b, spin_z_0, '
#                 'kappa_a_prime, kappa_b_prime, kappa_a, kappa_b, g, mu,'
#                 'spin_total')
#
#     wat = omega_a + u_a * spin_z
#     wbt = omega_b + u_b * spin_z
#     mu = (wat - wbt) / 2
#     watt = wat - mu
#     wbtt = wbt + mu
#     kap = kappa_a / 2
#     kbp = kappa_b / 2
#
#     equation = Poly(
#         ((omega_0 - mu) * (watt ** 2 + kap ** 2) * (wbtt ** 2 + kbp ** 2) /
#          (g ** 2))
#         + 2 * ((watt - I * kap) * (wbtt ** 2 + kbp ** 2)
#                + (wbtt + I * kbp) * (watt ** 2 + kap ** 2)) * spin_z
#         + ((spin_total ** 2) - (spin_z ** 2)) * (
#             u_a * (wbtt ** 2 + kbp ** 2) + u_b * (watt ** 2 + kap ** 2)),
#         spin_z)
#
#     coefficient_list = equation.all_coeffs()
#
#     analytical_coefficients = [
#         simplify(coefficient) for coefficient in coefficient_list]
#
#     plog(s, f"Analytical coefficients:\n{analytical_coefficients}", 2)
#
#     return analytical_coefficients


# def stationary_spin_z_coefficients(s):
#     w0, wat, wbt, wa, wb, ua, ub, sz, kap, kbp, ka, kb, ga, gb, gap, gbp, st, \
#     phi = symbols(
#         'omega_0, omega_a_tilde, omega_b_tilde, omega_a, omega_b, u_a, u_b, '
#         'spin_z_0, kappa_a_prime, kappa_b_prime, kappa_a, kappa_b, g_a, g_b, '
#         'g_a_p, g_b_p, spin_total, phi')
#
#     wat = wa + ua * sz
#     wbt = wb + ub * sz
#     kap = ka / 2
#     kbp = kb / 2
#
#     sp = sqrt(st ** 2 - sz ** 2) * exp(I * phi)
#     # sm = conjugate(sp)
#     sm = sqrt(st ** 2 - sz ** 2) * exp(- I * phi)
#
#     expression = Eq(
#         ga * ((ga * sp + gap * sm) * sm / (wat + I * kap) -
#               (ga * sm + gap * sp) * sp / (wat - I * kap)) +
#         gap * (-(ga * sp + gap * sm) * sp / (wat + I * kap) +
#                (ga * sm + gap * sp) * sm / wat - I * kap) +
#         gb * (-(gb * sm + gbp * sp) * sp / (wbt + I * kbp) +
#               (gb * sp + gbp * sm) * sm / (wbt - I * kbp)) +
#         gbp * ((gb * sm + gbp * sp) * sm / (wbt + I * kbp) -
#                (gb * sp + gbp * sm) * sp / (wbt - I * kbp)),
#         0)
#
#     equation = solve(expression, sz)
#
#     return equation

# def stationary_spin_z_coefficients(s):
#     w0, wat, wbt, wa, wb, ua, ub, sz, kap, kbp, ka, kb, ga, gb, gap, gbp, st, \
#     phi, a, b, ac, bc, ams, bms = symbols(
#         'omega_0, omega_a_tilde, omega_b_tilde, omega_a, omega_b, u_a, u_b, '
#         'spin_z_0, kappa_a_prime, kappa_b_prime, kappa_a, kappa_b, g_a, g_b, '
#         'g_a_p, g_b_p, spin_total, phi, alpha_0, beta_0, alpha_conjugate,'
#         'beta_conjugate, alpha_mod_square, beta_mod_square')
#
#     wat = wa + ua * sz
#     wbt = wb + ub * sz
#     kap = ka / 2
#     kbp = kb / 2
#
#     sp = sqrt(st ** 2 - sz ** 2) * exp(I * phi)
#     sm = conjugate(sp)
#
#     a = - ((ga * sm + gap * sp) / (wat - I * kap))
#     b = - ((gb * sp + gbp * sm) / (wbt - I * kbp))
#
#     ac = conjugate(a)
#     bc = conjugate(b)
#
#     ams = a * ac
#     bms = b * bc
#
#     expression1 = Eq(
#         (w0 + ua * ams + ub * bms) * sp,
#         2 * (ga * ac + gap * a + gb * b + gbp * bc) * sz)
#
#     expression2 = expression1.subs([
#         ('kappa_b', 'kappa_a'), ('g_b', 'g_a'), ('g_b_p', 'g_a_p'), ('u_a', 0),
#         ('u_b', 0)])
#
#     # print(expression2)
#
#     equation = solve(expression2, sz)
#
#     return equation


def find_roots(s, p, coefficients, omega_a, omega_b, g):
    # coefficients_numerical = \
    #     [coefficient.evalf(
    #         subs={"omega_a": omega_a, "omega_b": omega_b, "g": g,
    #               "omega_0": p.omega_0, "u_a": p.u_a, "u_b": p.u_b,
    #               "kappa_a": p.kappa_a, "kappa_b": p.kappa_b,
    #               "spin_total": p.spin_total})
    #         for coefficient in coefficients]

    coefficients_numerical = \
        eval(str(coefficients), None,
             {"omega_a": omega_a, "omega_b": omega_b, "g": g,
              "omega_0": p.omega_0, "u_a": p.u_a, "u_b": p.u_b,
              "kappa_a": p.kappa_a, "kappa_b": p.kappa_b, "I": 1.j,
              "spin_total": p.spin_total})

    plog(s, f"Numerical coefficients:\n{coefficients_numerical}", 2)

    return roots(coefficients_numerical)


if __name__ == "__main__":
    start_time = clock()
    s = program_configuration()

    plog(s, "Solving for analytic forms of polynomial coefficients of spin_z "
            "quartic equation:\n")

    # regimes = ("sra", "srb", "zero_g_prime")

    # for regime in regimes:
    #     plog(s, f"{regime}...")
    #     name = getattr(s, f"coefficients_{regime}")
    #     exit(name)
    #     coefficients = globals()[f"{regime}_spin_z_coefficients"](s)
    #     plog(s, f"{INDENT}Calculation done.")
    #     write_out_raw(s, coefficients, name)
    #     plog(s, "")

    # spin_z = i_gamma_spin_z_coefficients(s)
    # print(spin_z)
    # print('')
    # write_out_raw(s, spin_z, 'pre_run_data/coefficients_spin_z_i_gamma.pkl')

    # from auxiliary import read_file_raw
    #
    # data = read_file_raw(s, 'pre_run_data/stationary_spin_z.pkl')
    # print(data)
    # exit()

    plog(s, f"Complete.\nRuntime: {stopwatch(start_time)} s\n{LINE}")
