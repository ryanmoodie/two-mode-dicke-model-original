# Simulating continuous symmetry breaking with cold atoms in an optical cavity

* Online repository at: https://bitbucket.org/ryanmoodie/two-mode-dicke-model-original

* This program aims to map out the phase diagram of an extended two-mode-cavity 
Dicke model.

* Written in Python (3.6+), using packages Scipy, Numpy, Sympy, Matplotlib and tqdm. 

### Settings notes ###

* Defaults are in defaults.py. Run this module to (re)generate settings files.  
* all frequencies are in MHz
* all times are in microseconds
