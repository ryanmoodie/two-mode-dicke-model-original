#!/usr/bin/env python
# coding=utf-8

"""
phase_finder_spaces.py
Module to define functions which use functions from phase_finder_core.py module
and have parameters filled appropriately for use by phase_finder_regimes.py
module.
"""

from functools import partial

import matrices
import phase_finder_core

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "01 Feb 2017"


def point(s, p, spin_z, phi, coefficients="", normal=False):
    return phase_finder_core.check_single_point(
        spin_z, matrices.perturbation_matrix,
        {'s': s, "p": p, "phi": phi(p.omega_a, p.omega_b, p.kappa_a, p.gamma),
         "omega_a": p.omega_a, "omega_b": p.omega_b, "g_a": p.g_a,
         "g_b": p.g_b, "g_a_p": p.g_a_prime, "g_b_p": p.g_b_prime,
         "u_a": p.u_a, "u_b": p.u_b, "coefficients": coefficients,
         "gamma": p.gamma, "normal": normal})


def solve_modes_g(s, p, g_p, spin_z_func, phi, coefficients="", normal=False):
    return phase_finder_core.solve_modes(
        s, p.g_list_modes, spin_z_func, lambda g: (
            {'s': s, "p": p, "omega_a": p.omega_a,
             "phi": phi(p.omega_a, p.omega_b, p.kappa_a, p.gamma),
             "omega_b": p.omega_a, "coefficients": coefficients,
             "g_a": g, "g_b": g, "g_a_p": g_p(g), "g_b_p": g_p(g),
             "u_a": p.u_a, "u_b": p.u_b, "gamma": p.gamma,
             "normal": normal},))


def solve_modes_omega(
        s, p, omega_b, phi, spin_z_func, coefficients="", normal=False):
    return phase_finder_core.solve_modes(
        s, p.omega_list_modes, spin_z_func, lambda omega: (
            {'s': s, "p": p, "omega_a": omega,
             "phi": phi(omega, omega_b(omega), p.kappa_a, p.gamma),
             "omega_b": omega_b(omega), "coefficients": coefficients,
             "g_a": p.g_a, "g_b": p.g_b, "g_a_p": p.g_a_prime,
             "g_b_p": p.g_b_prime, "u_a": p.u_a, "u_b": p.u_b,
             "gamma": p.gamma, "normal": normal},))


def check_region_omega_v_g(s, p, calculation, variables_function):
    return phase_finder_core.check_region(
        s, calculation, variables_function, p.omega_list, p.g_list)


def check_region_omega_v_gamma(s, p, calculation, variables_function):
    return phase_finder_core.check_region(
        s, calculation, variables_function, p.omega_list, p.gamma_list)


def check_region_omega_a_v_omega_b(s, p, calculation, variables_function):
    return phase_finder_core.check_region(
        s, calculation, variables_function, p.omega_list, p.omega_b_list)


def solve_state_stability_omega_v_g(
        s, p, spin_z, omega_b, gamma, g_p, phi, coefficients="", normal=False):
    return check_region_omega_v_g(
        s, p, partial(phase_finder_core.check_point_stable, spin_z,
                      matrices.perturbation_matrix),
        lambda omega, g: (
            {'s': s, "p": p, "omega_a": omega,
             "phi": phi(omega, omega_b(omega), p.kappa_a, gamma),
             "coefficients": coefficients, "omega_b": omega_b(omega),
             "g_a": g, "g_b": g, "g_a_p": g_p(g), "g_b_p": g_p(g),
             "u_a": p.u_a, "u_b": p.u_b, "gamma": gamma, "normal": normal},))


def solve_state_stability_omega_v_g_advanced(
        s, p, spin_z, omega_b, g_p, phi, coefficients="", normal=False):
    return check_region_omega_v_g(
        s, p, partial(phase_finder_core.check_point_stable_advanced, spin_z,
                      matrices.perturbation_matrix),
        lambda omega, g: (
            {'s': s, "p": p, "omega_a": omega,
             "phi": phi(omega, omega_b(omega), p.kappa_a, p.gamma),
             "coefficients": coefficients, "omega_b": omega_b(omega),
             "g_a": g, "g_b": g, "g_a_p": g_p(g), "g_b_p": g_p(g),
             "u_a": p.u_a, "u_b": p.u_b, "gamma": p.gamma, "normal": normal},))


def solve_state_stability_xyz_omega_v_g(s, p, spin_x, spin_y, spin_z, omega_b,
                                        g_p, coefficients="", normal=False):
    return check_region_omega_v_g(
        s, p, partial(phase_finder_core.check_point_stable_xyz, spin_x, spin_y,
                      spin_z, matrices.perturbation_matrix_xyz),
        lambda omega, g: (
            {'s': s, "p": p, "omega_a": omega,
             "omega_b": omega_b(omega), "g_a": g, "g_b": g,
             "g_a_p": g_p(g), "g_b_p": g_p(g), "u_a": p.u_a, "u_b": p.u_b,
             "coefficients": coefficients, "normal": normal},))


def solve_state_stability_omega_v_gamma(
        s, p, spin_z, omega_b, g, phi, coefficients="", normal=False):
    return check_region_omega_v_gamma(
        s, p, partial(phase_finder_core.check_point_stable, spin_z,
                      matrices.perturbation_matrix),
        lambda omega_a, gamma: (
            {'s': s, "p": p, "omega_a": omega_a,
             "phi": phi(omega_a, omega_b(omega_a), p.kappa_a, gamma),
             "coefficients": coefficients, "omega_b": omega_b(omega_a),
             "g_a": g, "g_b": g, "g_a_p": 1.j * gamma * g,
             "g_b_p": 1.j * gamma * g, "u_a": p.u_a, "u_b": p.u_b,
             "gamma": gamma, "normal": normal},))


def solve_state_stability_omega_v_gamma_advanced(
        s, p, spin_z, omega_b, g, phi, coefficients="", normal=False):
    return check_region_omega_v_gamma(
        s, p, partial(phase_finder_core.check_point_stable_advanced, spin_z,
                      matrices.perturbation_matrix),
        lambda omega_a, gamma: (
            {'s': s, "p": p, "omega_a": omega_a,
             "phi": phi(omega_a, omega_b(omega_a), p.kappa_a, gamma),
             "coefficients": coefficients, "omega_b": omega_b(omega_a),
             "g_a": g, "g_b": g, "g_a_p": 1.j * gamma * g,
             "g_b_p": 1.j * gamma * g, "u_a": p.u_a, "u_b": p.u_b,
             "gamma": gamma, "normal": normal},))


def solve_state_stability_omega_a_v_omega_b(
        s, p, spin_z, g, g_p, gamma, phi, coefficients="", normal=False):
    return check_region_omega_a_v_omega_b(
        s, p, partial(phase_finder_core.check_point_stable, spin_z,
                      matrices.perturbation_matrix),
        lambda omega_a, omega_b: (
            {'s': s, "coefficients": coefficients,
             "p": p, "phi": phi(omega_a, omega_b, p.kappa_a, p.gamma),
             "omega_a": omega_a, "omega_b": omega_b,
             "g_a": g, "g_b": g, "g_a_p": g_p(g, gamma),
             "g_b_p": g_p(g, gamma), "u_a": p.u_a,
             "u_b": p.u_b, "gamma": gamma, "normal": normal},))


def solve_state_stability_omega_a_v_omega_b_advanced(
        s, p, spin_z, g, g_p, phi, coefficients="", normal=False):
    return check_region_omega_a_v_omega_b(
        s, p, partial(phase_finder_core.check_point_stable_advanced, spin_z,
                      matrices.perturbation_matrix),
        lambda omega_a, omega_b: (
            {'s': s, "coefficients": coefficients,
             "p": p, "phi": phi(omega_a, omega_b, p.kappa_a, p.gamma),
             "omega_a": omega_a, "omega_b": omega_b,
             "g_a": g, "g_b": g, "g_a_p": g_p, "g_b_p": g_p, "u_a": p.u_a,
             "u_b": p.u_b, "gamma": p.gamma, "normal": normal},))


def solve_state_stability_xyz_omega_a_v_omega_b(
        s, p, spin_x, spin_y, spin_z, g, g_p, coefficients="", normal=False):
    return check_region_omega_a_v_omega_b(
        s, p, partial(phase_finder_core.check_point_stable_xyz, spin_x, spin_y,
                      spin_z, matrices.perturbation_matrix_xyz),
        lambda omega_a, omega_b: (
            {'s': s, "p": p, "omega_a": omega_a,
             "omega_b": omega_b, "g_a": g, "g_b": g, "g_a_p": g_p,
             "g_b_p": g_p, "u_a": p.u_a, "u_b": p.u_b,
             "coefficients": coefficients, "normal": normal},))


if __name__ == "__main__":
    exit()
