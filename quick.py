#!/usr/bin/env python
# coding=utf-

"""
quick.py
Run to calculate and plot in one go.
"""

from main import main
from plot import plot

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "12 Feb 2017"

if __name__ == '__main__':
    main()
    plot()
