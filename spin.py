#!/usr/bin/env python
# coding=utf-8

"""
spin.py
Module containing functions to generate the spin_z values.
"""

from cmath import sqrt as csqrt
from cmath import exp
from math import copysign
from sys import exit

from numpy import arccos

from auxiliary import plog
from spin_numerics import find_roots

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "22 Mar 2017"


def inverted(p, **params):
    return [p.spin_total]


def normal(p, **params):
    return [-p.spin_total]


def _cut_off(p, spin):
    return copysign(p.spin_total, spin) if (abs(spin) > p.spin_total) \
        else spin


def _root_signs(p, spin):
    return [spin(root_sign) for root_sign in (-1, 1)]


def numeric_spin(
        s, p, coefficients, omega_a, omega_b, g_a, **params):
    roots = find_roots(s, p, coefficients, omega_a, omega_b, g_a)
    plog(s, f"roots:\n{roots}", 2)
    spins = [_cut_off(p, root.real) for root in roots if
             abs(root.imag) < p.zero]
    plog(s, f"spin_z: {spins}", 2)
    return spins


def x_g_prime_equals_g_srb(
        s, p, omega_a, omega_b, g_a, spin_z_0, u_a, u_b, **params):
    if omega_a == -omega_b:
        return [sign * p.spin_total for sign in (-1, 1)]

    wat = omega_a + u_a * spin_z_0
    wbt = omega_b + u_b * spin_z_0

    kap = p.kappa_a / 2
    kbp = p.kappa_b / 2

    def spin_x(root_sign):
        return root_sign * 0.5 * csqrt(
            - p.omega_0 * (wat ** 2. + kap ** 2.)
            * (wbt ** 2. + kbp ** 2.) / (
                u_a * (wbt ** 2 + kbp ** 2)
                + u_b * (wat ** 2 + kap ** 2))
        ) / g_a

    roots = _root_signs(p, spin_x)

    spins = [_cut_off(p, root.real) for root in roots if
             abs(root.imag) < p.zero]

    plog(s, f"spin_x: {spins}", 2)

    return spins


def y_g_prime_equals_g_srb(s, p, spin_x_0, spin_z_0, **params):
    def spin_y(root_sign):
        return root_sign * csqrt(
            p.spin_total ** 2. - spin_x_0 ** 2. - spin_z_0 ** 2.)

    roots = _root_signs(p, spin_y)

    spins = [_cut_off(p, root.real) for root in roots if
             abs(root.imag) < p.zero]

    plog(s, f"spin_y: {spins}", 2)

    return spins


def z_g_prime_equals_i_gamma_g(
        s, p, omega_a, omega_b, g_a, gamma, phi, **params):
    # U=0
    kappa = p.kappa_a
    kappa_prime = 0.5 * kappa
    g = g_a
    omega_0 = p.omega_0

    if phi is None:
        if s.debug:
            plog(s, "DEBUG: phi solution doesn't exist")
        return []

    if (not gamma) and p.allow_rotating_solution:
        omega_av = 0.5 * (p.omega_a + p.omega_b)
        try:
            spin_z = -p.omega_0 * (omega_av ** 2 + kappa_prime ** 2) / \
                     (4 * omega_av * g ** 2)
        except ZeroDivisionError:
            spin_z = p.spin_total
        plog(s, f"spin_z: {spin_z}", 2)
        return [spin_z]

    spin_z = \
        -omega_0 / (2 * g ** 2) * \
        ((((1 + gamma ** 2) * omega_a - (1 - gamma ** 2) * 1.j * kappa_prime -
           2 * 1.j * gamma * omega_a * exp(-1.j * 2 * phi))
          / (omega_a ** 2 + kappa_prime ** 2)) +
         (((1 + gamma ** 2) * omega_b + (1 - gamma ** 2) * 1.j * kappa_prime +
           2 * 1.j * gamma * omega_b * exp(- 1.j * 2 * phi))
          / (omega_b ** 2 + kappa_prime ** 2))) ** (-1)

    plog(s, f"spin_z: {spin_z}", 2)

    if abs(spin_z.imag) > p.zero:
        if s.debug:
            plog(s, f"Complex spin_z: {spin_z}")
        return []

    return [_cut_off(p, spin_z.real)]


def phi_g_prime_equals_i_gamma_g(omega_a, omega_b, kappa_a, gamma):
    if (omega_a == omega_b) or ((not gamma) and False):
        return 0.

    kappa_prime = kappa_a / 2

    numerator = -(1 - gamma ** 2) * kappa_prime * (omega_a + omega_b)
    denominator = 2 * gamma * (omega_a * omega_b - kappa_prime ** 2)

    if abs(numerator) > abs(denominator):
        return None

    return 0.5 * arccos(numerator / denominator)


def phi_g_prime_equals_i_gamma_g_sra(omega_a, omega_b, kappa_a, gamma):
    try:
        return -phi_g_prime_equals_i_gamma_g(omega_a, omega_b, kappa_a, gamma)
    except TypeError:
        return None


if __name__ == "__main__":
    exit()
