#!/usr/bin/env python
# coding=utf-8

"""
phase_finder_core.py
Module containing functions to solve the system of equations.
"""

from cmath import exp
from cmath import sqrt as csqrt
from functools import partial
from multiprocessing import Pool
from time import clock, sleep

from numpy import reshape, lexsort
from scipy.linalg import eigvals

import matrices
from auxiliary import plog, INDENT, stopwatch_pretty, verbose_parameters

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "05 Jul 2017"


def check_region(s, calculation, variables_function, var_i_list, var_j_list):
    start_time = clock()
    plog(s, f"{INDENT}Main calculation under way...")

    with Pool(processes=s.number_of_workers) as pool:
        def update(percentage_):
            plog(s, f"\r{INDENT*2}{percentage_} %", flush=True, end="")

        r = pool.starmap_async(
            calculation,
            [element for row in [
                [variables_function(var_i, var_j) for var_j in var_j_list]
                for var_i in var_i_list]
             for element in row])

        total = r._number_left
        percentage = 0
        update(percentage)

        while not r.ready():
            remaining = r._number_left
            new_percentage = int(100 * (total - remaining) / total)
            if new_percentage != percentage:
                percentage = new_percentage
                update(percentage)
            sleep(1.)

        update(100)
        result = r.get()

    result = reshape(result, (len(var_i_list), len(var_j_list)))

    plog(s,
         f"\n{INDENT*2}Main calculation done ({stopwatch_pretty(start_time)}).")

    return result


def find_eigenvalues_raw(matrix, args, order=False):
    eigenvalues = \
        eigvals(matrix(**args), overwrite_a=False, check_finite=False)

    if order:
        ordering = lexsort(([x.real for x in eigenvalues],
                            [x.imag for x in eigenvalues]))
        eigenvalues = [eigenvalues[i] for i in ordering]

    plog(args['s'], f"Eigenvalues:\n{eigenvalues}", 2)

    return eigenvalues


def check_point_stable_raw(matrix, args):
    return max(find_eigenvalues_raw(matrix, args).imag) <= args['p'].zero


def find_eigenvalues(spin_z_func, params):
    spin_zs = spin_z_func(**params)
    try:
        [params["spin_z_0"]] = spin_zs
    except ValueError:
        print("Spin_z doesn't exist!")
        return []

    params["spin_plus_0"] = spin_plus(params)
    params["alpha_0"] = alpha(params)
    params["beta_0"] = beta(params)

    return find_eigenvalues_raw(
        matrices.perturbation_matrix, params, order=True)


def solve_modes(s, g_list, spin_z_func, variables_function):
    with Pool(processes=s.number_of_workers) as pool:
        result = pool.starmap(
            partial(find_eigenvalues, spin_z_func),
            [variables_function(g) for g in g_list])
    return result


def spin_plus(params):
    for key, value in params.items():
        globals()[key] = value

    return csqrt((p.spin_total ** 2.) - (spin_z_0 ** 2.)) * exp(1.j * phi)


def alpha(params):
    for key, value in params.items():
        globals()[key] = value

    return - ((g_a * spin_plus_0.conjugate() + g_a_p * spin_plus_0) /
              (omega_a + u_a * spin_z_0 - 0.5j * p.kappa_a))


def beta(params):
    for key, value in params.items():
        globals()[key] = value

    return - ((g_b * spin_plus_0 + g_b_p * spin_plus_0.conjugate()) /
              (omega_b + u_b * spin_z_0 - 0.5j * p.kappa_b))


def check_point_stable(spin_z_func, matrix, params):
    debug = True
    if params["phi"] is None:
        if debug:
            print("DEBUG: No phi!")
        return False
    spin_zs = spin_z_func(**params)
    if spin_zs == [] and debug:
        print("DEBUG: No spin_z!")
    stable = []
    for spin_z in spin_zs:
        params["spin_z_0"] = spin_z
        params["spin_plus_0"] = spin_plus(params)
        params["alpha_0"] = alpha(params)
        params["beta_0"] = beta(params)
        if abs(spin_z) < params['p'].spin_total or params['normal']:
            stable.append(check_point_stable_raw(matrix, params))
    return any(stable)


def check_point_stable_advanced(spin_z_func, matrix, params):
    """
    Assumes a unique stable superradiant solution!
    """
    s = params["s"]
    p = params["p"]

    if params["phi"] is None:
        if s.debug:
            plog(s, "DEBUG: phi solution doesn't exist")
        return False

    try:
        [spin_z] = spin_z_func(**params)

        if abs(spin_z) < params['p'].spin_total:
            params["spin_z_0"] = spin_z
            params["spin_plus_0"] = spin_plus(params)
            params["alpha_0"] = alpha(params)
            params["beta_0"] = beta(params)

            stable = check_point_stable_raw(matrix, params)

            if stable:
                if abs(params["beta_0"]) > abs(params["alpha_0"]) + p.zero:
                    return 2
                if abs(params["alpha_0"]) > abs(params["beta_0"]) + p.zero:
                    return 3
                else:
                    return 1

            if s.debug:
                plog(s, "DEBUG: spin_z existed, but unstable "
                        "(for SR and SR$\\beta$)")

        if s.debug:
            plog(s, "DEBUG: |spin_z| > spin_total (for SR and SR$\\beta$)")

    except ValueError:
        if s.debug:
            plog(s,
                 "DEBUG: spin_z solution doesn't exist for SR$\\beta$ or SR")

    params["phi"] = -params["phi"]
    try:
        [spin_z] = spin_z_func(**params)

        if abs(spin_z) < params['p'].spin_total:
            params["spin_z_0"] = spin_z
            params["spin_plus_0"] = spin_plus(params)
            params["alpha_0"] = alpha(params)
            params["beta_0"] = beta(params)

            if check_point_stable_raw(matrix, params):
                if abs(params["alpha_0"]) > abs(params["beta_0"]) + p.zero:
                    return 3
                if abs(params["beta_0"]) > abs(params["alpha_0"]) + p.zero:
                    return 2
                else:
                    return 1

            if s.debug:
                plog(s,
                     "DEBUG: spin_z existed, but unstable (for SR$\\alpha$)")

        if s.debug:
            plog(s, "DEBUG: |spin_z| > spin_total (for SR$\\alpha$)")

    except ValueError:
        if s.debug:
            plog(s, "DEBUG: spin_z solution doesn't exist (for SR$\\alpha$)")
        return 0

    return 0


def check_point_stable_xyz(
        spin_x_func, spin_y_func, spin_z_func, matrix, params):
    spin_zs = spin_z_func(**params)
    stable = []
    for spin_z in spin_zs:
        params["spin_z_0"] = spin_z
        spin_xs = spin_x_func(**params)
        for spin_x in spin_xs:
            if spin_x ** 2 + spin_z ** 2 < params['p'].spin_total ** 2:
                params["spin_x_0"] = spin_x
                spin_ys = spin_y_func(**params)
                for spin_y in spin_ys:
                    params["spin_y_0"] = spin_y
                    stable.append(check_point_stable_raw(matrix, params))
    return any(stable)


def check_single_point(spin_z, matrix, params):
    stable = check_point_stable(spin_z, matrix, params)
    verbose_raw = "stable" if stable else "unstable"

    if True:
        for keyword, argument in params.items():
            globals()[keyword] = argument

        try:
            alpha_0 = params["alpha_0"]
            beta_0 = params["beta_0"]
            spin_plus_0 = params["spin_plus_0"]
            spin_z_0 = params["spin_z_0"]
            params["|alpha_0|^2"] = (alpha_0 * alpha_0.conjugate()).real
            params["|beta_0|^2"] = (beta_0 * beta_0.conjugate()).real
        except KeyError:
            pass

        if False:
            print([alpha_0, beta_0, spin_plus_0, spin_z_0])

    return f"State is {verbose_raw}.\n{verbose_parameters(params)}"


if __name__ == "__main__":
    exit()
