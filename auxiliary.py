#!/usr/bin/env python
# coding=utf-8

"""
auxiliary.py
Module containing functions to read from/write to disk and other odds and ends.
"""

from pickle import dump, load
from time import clock

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "01 Feb 2017"

LINE = '\n------------------------------------------------------------------' \
       '-------------\n'
INDENT = "  "


def write_out_raw(s, data, file_name):
    plog(s, f"Writing data to file '{file_name}'...")

    with open(file_name, 'wb') as output_file:
        dump(data, output_file)

    plog(s, f"{INDENT}Done writing to file.")


def read_file_raw(s, file_name):
    plog(s, f'Reading data from file "{file_name}"...', 1)

    with open(file_name, 'rb') as input_file:
        data = load(input_file)

    plog(s, f"{INDENT}Done reading file.", 1)

    return data


def write_out(s, p, data, identifier):
    write_out_raw(
        s, (p, data), f"{s.write_file_start}{identifier}{s.write_file_end}")


def read_file(s, identifier):
    return read_file_raw(
        s, f"{s.read_file_start}{identifier}{s.read_file_end}")


def save_plot_to_disk(s, fig, name):
    plog(s, f"Saving plot '{name}' to disk...")
    fig.savefig(s.graph_file_start + name + s.graph_file_end,
                bbox_inches='tight', format='pdf', transparent=True,
                frameon=False)
    plog(s, f"{INDENT}Done saving plot '{name}' to disk.")


def plog(s, text="", verbose_level=0, **kwargs):
    if s.verbose_level >= verbose_level:
        print(text, **kwargs)
    if s.logging and verbose_level <= s.log_level:
        with open(f"{s.log_file_start}{s.write_file_name}{s.log_file_end}",
                  'a') as file:
            file.write(f"{text}\n")


def stopwatch(start_time):
    return round(clock() - start_time, 3)


def stopwatch_pretty(start_time):
    return f"{stopwatch(start_time)} s"


def verbose_parameters(parameters_dictionary):
    info = "At:\n"
    for keyword, argument in parameters_dictionary.items():
        if isinstance(argument, (int, float)):
            info += f"{INDENT}{keyword}={round(argument, 5)}\n"
        elif isinstance(argument, complex):
            info += f"{INDENT}{keyword}={argument}\n"
    return info


if __name__ == "__main__":
    exit()
