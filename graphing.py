#!/usr/bin/env python
# coding=utf-8

"""
Graphs.py
Module containing functions to plot graphs of results.
"""

from cmath import log
from itertools import combinations
from math import sqrt

from matplotlib.colors import ListedColormap
from matplotlib.pyplot import subplots, tight_layout, figure
from mpl_toolkits.mplot3d import Axes3D
from numpy import array, empty, linspace, empty_like, transpose, meshgrid, \
    pi, cos, sin, ceil
from pylab import cm
from scipy.constants import golden

from auxiliary import plog
from configurer import graph_settings
from defaults import regimes as defaults_regimes

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "01 Feb 2017"

g = graph_settings()

OMEGA = "$\\omega$"
OMEGA_A = "$\\omega_a$"
OMEGA_B = "$\\omega_b$"
G = "$g$"
GAMMA = "$\\gamma$"

DEFAULT_RATIO = 1 / golden


class OneDimensionalPlots:
    @staticmethod
    def one_dimensional_plot(
            x_label, y_label, grid=False, number_of_rows=1,
            number_of_columns=1, aspect=DEFAULT_RATIO, slim_dim=False):
        w = g.width
        h = aspect * w
        fig, axss = subplots(
            nrows=number_of_rows, ncols=number_of_columns, figsize=(w, h))

        for line in (number_of_rows, number_of_columns):
            if line == 1:
                axss = [axss]

        for row in axss:
            for ax in row:
                ax.set_xlabel(x_label)
                ax.set_ylabel(y_label)

                if grid:
                    ax.grid()

        if slim_dim:
            for line in (number_of_rows, number_of_columns):
                if line == 1:
                    [axss] = axss

        return fig, axss

    def plot_modes(self, p, datas, x_list, x_name):
        comps = ("real", "imag") if g.plot_real else ("imag",)
        syms = ("Re", "Im") if g.plot_real else ("Im",)

        ncols = len(g.draw) if g.plot_separate else len(comps)
        nrows = len(comps) if g.plot_separate else 1

        fig, axss = self.one_dimensional_plot(
            x_name, "$\\lambda_i$", g.show_grid, number_of_columns=ncols,
            number_of_rows=nrows)

        [data] = datas

        edge_top = max([max([max([
            mode.imag, mode.real]) if g.plot_real else abs(mode.imag)
                             for i, mode in enumerate(point, start=1)
                             if i in g.draw]) for point in data])
        edge_bottom = min([min([min([
            mode.imag, mode.real]) if g.plot_real else abs(mode.imag)
                                for i, mode in enumerate(point, start=1)
                                if i in g.draw]) for point in data])

        edge = max([abs(x) for x in [edge_top, edge_bottom]])
        if edge < 1e-2:
            data = [[1000 * mode for mode in point] for point in data]
            edge = edge * 1050
            unit = "kHz"
        else:
            edge = edge * 1.05
            unit = "MHz"

        if g.plot_separate:

            j = 0
            for i, datum in enumerate(transpose(data)):
                if i + 1 in g.draw:
                    for axs, comp, sym in zip(axss, comps, syms):
                        modes = [getattr(d, comp) for d in datum]
                        axs[j].plot(
                            x_list, modes)
                        axs[j].set_ylabel(f"$\\{sym}(\\lambda_{i+1})$")
                    j += 1

            for axs in axss:
                for ax in axs:
                    ax.set_ylim(-edge, edge)

        else:

            [axs] = axss

            cut_off = 0.95 * (p.kappa_a / 2)

            try:
                g_point = sqrt(
                    -(p.omega_0 * (p.omega_a ** 2 + (p.kappa_a / 2) ** 2)) /
                    (4 * p.spin_total * p.omega_a * (1 + p.gamma ** 2)))
            except ValueError:
                g_point = sqrt(
                    -(p.omega_0 * (p.omega_a ** 2 + (p.kappa_a / 2) ** 2)) /
                    (4 * -p.spin_total * p.omega_a * (1 + p.gamma ** 2)))

            point = {OMEGA_A: p.omega_a, G: g_point}[x_name.split()[0]]
            point2 = p.kappa_a ** 2 / (4 * p.omega_a)

            for ax in axs:
                if g.tools:
                    # ax.plot((x_list[0], x_list[-1]), (0, 0), "k-")
                    ax.plot((point, point), (-cut_off, cut_off), "k-")
                    if x_name == OMEGA:
                        ax.plot((0, 0), (-cut_off, cut_off), "k-")
                        ax.plot((point2, point2), (-cut_off, cut_off), "k-")

            for i, datum in enumerate(transpose(data)):
                if i + 1 in g.draw:
                    for comp, sym, ax in zip(comps, syms, axs):
                        if abs(getattr(datum[-1], comp)) < cut_off:
                            modes = [getattr(d, comp) for d in datum]
                            ax.plot(x_list, modes,
                                    label=i + 1)

            for ax, comp, sym in zip(axs, comps, syms):
                ax.legend(loc="best", title="$i=$")
                ax.set_ylabel(f"$\\{sym}(\\lambda_i)$ ({unit})")

        return [fig]

    def plot_modes_g(self, p, data):
        g_list = p.g_list_modes
        if g_list[-1] < 1e-2:
            g_list_edit = [x * 1000 for x in g_list]
            unit = "kHz"
        else:
            g_list_edit = g_list
            unit = "MHz"
        return self.plot_modes(p, data, g_list_edit, f"{G} ({unit})")

    def plot_modes_omega(self, p, data):
        return self.plot_modes(p, data, p.omega_list_modes, f"{OMEGA_A} (MHz)")

    def plot_simple_time_evolution(self, p, data):
        figures = []

        light_field_names = ["$\Re(\\alpha)$", "$\Im(\\alpha)$",
                             "$\Re(\\beta)$", "$\Im(\\beta)$"] \
            if g.plot_real_imag else ["${|\\alpha|}^2$",
                                      "${|\\beta|}^2$"]

        spin_names = ["$\Re(S_+)$", "$\Im(S_+)$", "$S_z$"] \
            if g.plot_real_imag else ["$S_x$", "$S_y$", "$S_z$"]

        if g.plot_bloch_sphere:
            aspect = 0.8
            w = g.width
            h = w * aspect
            fig_b = figure(figsize=(w, h))
            ax_b = Axes3D(fig_b)

            ax_b.set_xlabel(spin_names[0])
            ax_b.set_ylabel(spin_names[1])
            ax_b.set_zlabel(spin_names[2])

            ax_b.grid(False)

            if not g.bloch_axes:
                ax_b.set_axis_off()

            r = p.spin_total

            if g.plot_surface:
                num = 20

                polar = linspace(0., pi, num)
                azimuth = linspace(0., 2. * pi, num)

                polars, azimuths = meshgrid(polar, azimuth)

                x = r * sin(polars) * cos(azimuths)
                y = r * sin(polars) * sin(azimuths)
                z = r * cos(polars)

                ax_b.plot_surface(x, y, z, rstride=1, cstride=1, alpha=0.3)

            one_rev = 70
            num_rev = 2
            start = -one_rev * num_rev

        if len(data) > 1:
            for time_evolution, point in zip(data, p.omega_points):
                populations = [x[1] for x in time_evolution]

                spins = [
                    [getattr(population[num], comp) for population in
                     populations]
                    for num, comp in zip([2, 2, 3], ["real", "imag", "real"])]

                if g.plot_bloch_sphere:
                    ax_b.plot(*[x[start:] for x in spins], label=point)

            ax_b.legend(loc='upper left', title="($\\omega_a$, $\\omega_b$)=")
            figures.append(fig_b)

            if g.plot_against_time:
                print("Sorry, plotting against time not supported for "
                      "multiple points yet")

        else:

            [data] = data

            t, populations = [[datum[i] for datum in data] for i in range(2)]

            light_fields = sum(
                [[[getattr(population[i], comp) for population in populations]
                  for comp in ("real", "imag")] for i in range(2)], []) \
                if g.plot_real_imag else \
                [[abs(population[i]) ** 2 for population in populations]
                 for i in range(2)]

            spins = [
                [getattr(population[num], comp) for population in populations]
                for num, comp in zip([2, 2, 3], ["real", "imag", "real"])]

            spin_pluses = [pop[2] for pop in populations]
            phis = [1.j / 2 * log(spin_minus / spin_plus) for
                    spin_minus, spin_plus in zip(
                    [x.conjugate() for x in spin_pluses], spin_pluses)]
            if any([abs(phi.imag) > p.zero for phi in phis]):
                print("Warning! Complex phi!")
            phis = [phi.real for phi in phis]

            if g.plot_bloch_sphere:
                ax_b.set_xlabel(spin_names[0])
                ax_b.set_ylabel(spin_names[1])
                ax_b.set_zlabel(spin_names[2])

                ax_b.plot(*[x[start:] for x in spins])

                figures.append(fig_b)

            if g.plot_against_time:
                number = ((5 + (2 if g.plot_real_imag else 0))
                          if g.use_separate_axes else 2) \
                         + (1 if g.plot_phi else 0)
                fig, axs = self.one_dimensional_plot(
                    'Time ($\\mu$s)', 'Populations', g.show_grid,
                    slim_dim=True, number_of_columns=number)

                if g.use_separate_axes:

                    if g.plot_phi:
                        names = light_field_names + ["$\\phi$"] + spin_names
                        lists = light_fields + [phis] + spins
                    else:
                        names = light_field_names + spin_names
                        lists = light_fields + spins

                    for variable_list, name, ax_b in zip(lists, names, axs):
                        if g.use_log_scale:
                            ax_b.semilogy(t, [abs(x) for x in variable_list])
                            label = "$|" + name[1:-1] + "|$" if any(
                                [x < 0 for x in variable_list]) else name
                        else:
                            ax_b.plot(t, variable_list)
                            label = name

                        ax_b.set_title(label)

                        for ax in axs[-3:]:
                            ax.set_ylim(-p.spin_total, p.spin_total)

                else:

                    titles = ["Light fields", "Spin components"]
                    namess = [light_field_names, spin_names]
                    listss = [light_fields, spins]

                    if g.plot_phi:
                        titles.append("$\\phi$")
                        namess.append(["$\\phi$"])
                        listss.append([phis])

                    for lists, names, ax_b, title \
                            in zip(listss, namess, axs, titles):
                        for variable_list, name in zip(lists, names):
                            if g.use_log_scale:
                                ax_b.semilogy(
                                    t, [abs(x) for x in variable_list],
                                    label="$|" + name + "|$" if any(
                                        [x < 0 for x in variable_list])
                                    else name)
                            else:
                                ax_b.plot(t, variable_list, label=name)

                        ax_b.legend(loc=g.legend_location)
                        ax_b.set_title(title)

                    axs[1].set_ylim(-p.spin_total, p.spin_total)

                figures.append(fig)

        return figures


class TwoDimensionalPlots:
    @staticmethod
    def pcolormesh_fixed(ax, x_axis, y_axis, data, **kwargs):
        x_min = x_axis[0]
        x_max = x_axis[-1]
        x_step = x_axis[1] - x_min

        y_start = y_axis[0]
        y_end = y_axis[-1]
        y_step = y_axis[1] - y_start

        new_x_min = x_min - 0.5 * x_step
        new_x_max = x_max - 0.5 * x_step

        new_y_start = y_start - 0.5 * y_step
        new_y_end = y_end - 0.5 * y_step

        x_axis_fixed = linspace(
            new_x_min, new_x_max, len(x_axis), endpoint=True)
        y_axis_fixed = linspace(
            new_y_start, new_y_end, len(y_axis), endpoint=True)

        return ax.pcolormesh(x_axis_fixed, y_axis_fixed, data, **kwargs)

    def plot_multipanel_of_phase_diagrams(
            self, p, data, x_axis, y_axis, x_label, y_label):
        number = len(data)

        multipanel_columns = g.multipanel_columns \
            if number > g.multipanel_columns else number

        multipanel_rows = int(ceil(number / multipanel_columns))

        fig, axs = subplots(
            nrows=multipanel_rows, ncols=multipanel_columns)

        if number == 1:
            axs = [axs]

        if g.multipanel_columns > 1 and multipanel_rows > 1:
            axs = axs.flatten()

        # all_regimes = []
        for datum, ax in zip(data, axs):
            colour_map, component_regimes = \
                self.plot_single_phase_diagram(
                    ax, p, datum, x_axis, y_axis, x_label, y_label)

            # for regime in component_regimes:
            #     if regime not in all_regimes:
            #         all_regimes.append(regime)

        if g.colourbar:
            from matplotlib.colorbar import make_axes
            c_ax, kw = make_axes([ax for ax in axs])
            cbar = fig.colorbar(colour_map, cax=c_ax, **kw)
            tick_locations = [x + 0.5 for x in range(len(component_regimes))]
            cbar.set_ticks(tick_locations)
            cbar.set_ticklabels(component_regimes)

        return [fig]

    def plot_single_phase_diagram(
            self, ax, p, data, x_axis, y_axis, x_label, y_label):
        x_unit = "" if x_label == GAMMA else " (MHz)"

        if g.x_axis_g_root_n and g.x_axis_khz:
            exit('Define single x_axis unit!')

        if g.x_axis_g_root_n and x_label == G:
            x_axis = [x * sqrt(p.n) for x in x_axis]

        if g.x_axis_khz and x_label == G:
            if 1.e-3 <= x_axis[-1] < 1.:
                x_axis = [1.e3 * x for x in x_axis]
                x_unit = ' (kHz)'
            elif x_axis[-1] < 1.e-3:
                x_axis = [1.e6 * x for x in x_axis]
                x_unit = ' (Hz)'

        num_phases = len(data)

        maps, regimes = zip(*data)

        map_all = empty(shape=(len(y_axis), len(x_axis)))

        def assign(*args):
            n = 0
            for set_len_ in reversed(range(1, num_phases + 1)):
                for combination_ in combinations(args, set_len_):
                    if all(combination_):
                        return n
                    n += 1
            return n

        for i in range(len(y_axis)):
            for j in range(len(x_axis)):
                map_all[i, j] = \
                    assign(*[maps[k][i][j] for k in range(num_phases)])

        mod = r'$\sqrt{N}$' if g.x_axis_g_root_n else ''

        ax.set_xlabel(f"{x_label}{mod}{x_unit}")
        ax.set_ylabel(f"{y_label} (MHz)")

        if g.title:
            ax.set_title(f"Phase diagram in the {y_label} vs {x_label} plane")

        syms = \
            ['$\\Downarrow$', '$\\Uparrow$', 'SRA', 'SRB', 'SR',
             'SR$\\alpha$', 'SR$\\beta$', 'SR']

        symbols = {defaults_regime: symbol for defaults_regime, symbol in
                   zip(defaults_regimes, syms)}

        regimes_possible = []
        for set_len in reversed(range(1, num_phases + 1)):
            for combination in combinations(regimes, set_len):
                regimes_possible.append(
                    ' + '.join([symbols[state] for state in combination]))
        regimes_possible.append('?')

        regimes_all = [
            '$\\Uparrow$',
            '$\\Uparrow$ + SR',
            '$\\Downarrow$',
            '$\\Uparrow$ + $\\Downarrow$ + SR',
            '$\\Uparrow$ + SRA',
            '$\\Uparrow$ + SRB',
            '$\\Downarrow$ + SRA',
            '$\\Downarrow$ + SRB',
            'SRA',
            'SRB',
            'SR',
            'SRA + SRB',
            '$\\Uparrow$ + $\\Downarrow$ + SRA',
            '$\\Uparrow$ + $\\Downarrow$ + SRB',
            '$\\Uparrow$ + SRA + SRB',
            '$\\Downarrow$ + SRA + SRB',
            '$\\Uparrow$ + $\\Downarrow$',
            '$\\Uparrow$ + $\\Downarrow$ + SRA + SRB',
            '?',
            '$\\Downarrow$ + SR'
        ]

        if any(['SR$\\alpha$' or 'SR$\\beta$' in symb
                for symb in regimes_possible]):
            regimes_all = [
                '$\\Uparrow$',
                '',
                '$\\Downarrow$',
                '',
                '$\\Uparrow$ + SR$\\alpha$',
                '$\\Uparrow$ + SR$\\beta$',
                '$\\Downarrow$ + SR$\\alpha$',
                '$\\Downarrow$ + SR$\\beta$',
                'SR$\\alpha$',
                'SR$\\beta$',
                'SR',
                'SR$\\alpha$ + SR$\\beta$',
                '$\\Downarrow$ + $\\Uparrow$ + SR$\\alpha$',
                '$\\Downarrow$ + SR',
                '$\\Downarrow$ + $\\Uparrow$ + SR$\\beta$',
                '',
                '$\\Uparrow$ + SR',
                '$\\Downarrow$ + $\\Uparrow$',
                '?',
                '$\\Downarrow$ + $\\Uparrow$ + SR'
            ]

        cmap_all = cm.get_cmap('tab20', 20).colors

        cmap_dict = {regime: cmap for regime, cmap in
                     zip(regimes_all, cmap_all)}

        regimes_present = []
        cmap_present = []
        map_present = empty_like(map_all)
        j = 0

        for i, regime in enumerate(regimes_possible):
            if i in map_all:
                regimes_present.append(regime)
                cmap_present.append(cmap_dict[regime])
                for x in range(len(y_axis)):
                    for y in range(len(x_axis)):
                        if map_all[x, y] == i:
                            map_present[x, y] = j
                j += 1

        cmap = ListedColormap(cmap_present)

        colour_map = self.pcolormesh_fixed(
            ax, x_axis, y_axis, map_present, cmap=cmap, vmin=0,
            vmax=len(regimes_present))

        tight_layout()

        return colour_map, regimes_present

    def plot_phase_diagram_of_omega_v_g(self, p, data):
        return self.plot_multipanel_of_phase_diagrams(
            p, data, p.g_list, p.omega_list, G, OMEGA)

    def plot_phase_diagram_of_omega_v_gamma(self, p, data):
        return self.plot_multipanel_of_phase_diagrams(
            p, data, p.gamma_list, p.omega_list, GAMMA, OMEGA_A)

    def plot_phase_diagram_of_omega_a_v_omega_b(self, p, data):
        return self.plot_multipanel_of_phase_diagrams(
            p, data, p.omega_b_list, p.omega_list, OMEGA_B, OMEGA_A)


if __name__ == "__main__":
    exit()
