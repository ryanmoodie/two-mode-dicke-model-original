#!/usr/bin/env python
# coding=utf-8

"""
main.py
File to be executed to run main program, ie. solve for results and save the
data as pickle files in folder 'simulation_data'.
"""

from argparse import ArgumentParser
from os.path import isfile
from time import clock

import phase_finder_regimes
import time_evolver
from auxiliary import write_out, plog, stopwatch, LINE, INDENT, read_file
from configurer import program_configuration, system_parameters
from defaults import regimes, spaces, third_axes

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "25 Jan 2017"


def verbose_stuff(s, regime, space):
    name = f"{regime}_{space}"
    plog(s, f"Solving for: '{name}'...")
    if regime == "sr_g_prime_equals_i_gamma_g" \
            and space in spaces[3:-1]:
        return [name, f"sr_g_prime_equals_i_gamma_g_srb_{space}",
                f"sr_g_prime_equals_i_gamma_g_sra_{space}"]
    else:
        return [name]


def persistent_action(s, p, regime, space, solver):
    def check_value(a, b):
        try:
            result = all([ea == eb for ea, eb in zip(a, b)])
        except TypeError:
            result = a == b
        return result

    names = verbose_stuff(s, regime, space)
    if isfile(f"{s.write_file_start}{names[0]}{s.write_file_end}"):
        plog(s, f"{INDENT}File already exists.")
        if s.overwrite:
            plog(s, f"{INDENT*2}Will overwrite (overwrite is on).")
        else:
            old_p = read_file(s, names[0])[0]
            same = all([check_value(a, b) for a, b in
                        zip(vars(old_p).values(), vars(p).values())])
            if not same:
                plog(s, f"{INDENT*2}"
                        f"Old file has different parameters: will overwrite.")
            else:
                plog(s, f"{INDENT*2}Existing file has same parameters, "
                        f"so will be reused.")
                return None

    for ax3 in third_axes:
        if getattr(s, ax3):
            nom = f"{names[0]}_l_{ax3}"
            data = getattr(solver, nom)(s, p)
            plog(s, f"{INDENT}Done solving for '{nom}'.")
            if len(names) > 1:
                multiple = len(data) // len(names)
                for (datum, point), name in zip(data, names * multiple):
                    write_out(s, p, datum, f"{name}-{ax3}={point}")
            else:
                for datum, point in data:
                    write_out(s, p, datum, f"{names[0]}-{ax3}={point}")


def instant_action(s, p, regime, space, solver):
    [name] = verbose_stuff(s, regime, space)
    plog(s, getattr(solver, name)(s, p), -1)


def loop_regimes(s, p, space, regimes_, action):
    for regime in regimes_:
        if getattr(s, space) and getattr(s, regime):
            action(s, p, regime, space, phase_finder_regimes)


def main():
    start_time = clock()

    parser = ArgumentParser()
    parser.add_argument(
        '-s', '--program_configuration_file', type=str, default='')
    parser.add_argument('-p', '--parameters_file', type=str, default='')
    args = parser.parse_args()

    s = program_configuration(args.program_configuration_file)
    p = system_parameters(args.parameters_file)

    plog(s, "Main program initialising...")

    loop_regimes(s, p, "point", regimes, instant_action)

    if any([s.modes_g, s.modes_omega]) and not any(
            [s.omega_v_g, s.omega_v_gamma, s.omega_a_v_omega_b]):
        reduced_regimes = regimes
    elif not any([s.modes_g, s.modes_omega]) and any([
        s.omega_v_g, s.omega_a_v_omega_b, s.omega_v_gamma]):
        reduced_regimes = [
            regime for regime in regimes if regime not in (
                "sr_g_prime_equals_i_gamma_g_sra",
                "sr_g_prime_equals_i_gamma_g_srb")]
    elif s.time:
        reduced_regimes = []
    else:
        exit("Please do modes and phase diagrams in separate runs (sorry).")

    for space in spaces[1:-1]:
        loop_regimes(s, p, space, reduced_regimes, persistent_action)

    if s.time:
        persistent_action(s, p, "simple", "time_evolver", time_evolver)

    plog(s, f"Program complete.\nRuntime: {stopwatch(start_time)} s.\n{LINE}")


if __name__ == "__main__":
    main()
