#!/usr/bin/env python
# coding=utf-8

"""
time_evolver.py
Module containing functions to solve the time
evolution of the system of equations.
"""

from functools import partial
from multiprocessing import Pool
from time import clock

from scipy.integrate import ode
from tqdm import tqdm

from auxiliary import plog, stopwatch
from eom import equations_of_motion

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "21 Mar 2017"


def raw_time_evolver(initial_variables, initial_time, parameters):
    integrator = ode(equations_of_motion)
    integrator.set_integrator("zvode")
    # , rtol=1e-8, atol=1e-8)
    # , method="bdf") # stiff
    # , method="adams") # non-stiff
    integrator.set_initial_value(initial_variables, initial_time)
    integrator.set_f_params(parameters)

    return integrator


def time_evolve_to_a_point(
        initial_variables, initial_time, time_step, parameters):
    integrator = raw_time_evolver(initial_variables, initial_time, parameters)
    return integrator.integrate(initial_time + time_step)


def time_evolve(
        s, initial_variables, initial_time, time_step, final_time, parameters):
    start_time = clock()
    plog(s, "Integrating equations of motion to find time evolution...")
    integrator = raw_time_evolver(initial_variables, initial_time, parameters)
    time_evolution = [(initial_time, initial_variables)]

    with tqdm(total=(final_time - initial_time) / time_step) as pbar:
        while integrator.successful() and integrator.t <= final_time:
            t = integrator.t + time_step
            y = integrator.integrate(t)
            time_evolution.append((t, y))
            pbar.update()

    plog(s, f"Integration complete ({stopwatch(start_time)} s).")

    return time_evolution


def single_time_evolver(s, p, omega_point):
    omega_a, omega_b = omega_point
    parameters = [omega_a, omega_b, p.omega_0, p.g_a,
                  p.g_b, p.g_a_prime, p.g_b_prime, p.kappa_a, p.kappa_b,
                  p.u_a, p.u_b]

    return time_evolve(s, p.initial_variables, p.start_time,
                       p.time_step, p.end_time, parameters)


def simple_time_evolver(s, p):
    with Pool(processes=s.number_of_workers) as pool:
        time_evolutions = \
            pool.map(partial(single_time_evolver, s, p), p.omega_points)

    return time_evolutions


if __name__ == "__main__":
    exit()
