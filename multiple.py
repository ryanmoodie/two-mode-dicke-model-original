#!/usr/bin/env python
# coding=utf-8

from argparse import ArgumentParser
from os import system

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "27 Apr 2017"


def main():
    parser = ArgumentParser()
    parser.add_argument('-r', '--file_to_run', type=str, default='main',
                        help='main or plot')
    args = parser.parse_args()

    queue = [
        '_equal-gs-neg-equal-us',
        '_equal-gs-neg-opposite-us',
        '_equal-gs-pos-equal-us',
        '_equal-gs-u-zero',
        '_zero-g-prime',
        '_zero-g-prime-u-more-under',
        '_zero-g-prime-u-more-zoom'
        '_zero-g-prime-u-more-zoom-zoom'
        '_zero-g-prime-u-over',
        '_zero-g-prime-u-under'
    ]

    for i, entry in enumerate(queue, 1):
        print(f'Starting {i} of {len(queue)}: {entry}\n')
        system(f'python {args.file_to_run}.py -s {entry} -p {entry}')
        print(f'Done {i} of {len(queue)}\n')


if __name__ == "__main__":
    main()
