#!/usr/bin/env python
# coding=utf-8

"""
defaults.py
Module which stores default settings and can be run to generate settings files.
"""

from configparser import ConfigParser
from multiprocessing import cpu_count
from os.path import isfile

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "03 Apr 2017"

SETTINGS_FOLDER = "settings"
SYSTEM_PARAMETERS_FILE = "parameters"
GRAPH_SETTINGS_FILE = "graphing"
PROGRAM_CONFIGURATION_FILE = "program"
SETTINGS_FILE_SUFFIX = "cfg"


def get_settings_path(file):
    return f"{SETTINGS_FOLDER}/{file}.{SETTINGS_FILE_SUFFIX}"


def make_settings_file(defaults, file, comment="", force=False):
    path = get_settings_path(file)
    if not force:
        if isfile(path):
            if input("Warning: will overwrite existing files"
                     " - proceed? (yes) ") != "yes":
                return None

    config = ConfigParser(allow_no_value=True)
    config.add_section("header")
    config.set("header", f"# {file}.{SETTINGS_FILE_SUFFIX}")
    config.set("header", f"# {comment}")
    config.read_dict(defaults)

    with open(path, "w+") as config_file:
        config.write(config_file)


def main():
    force = input("Overwrite all settings files? (yes)") == "yes"
    make_settings_file(program_defaults, PROGRAM_CONFIGURATION_FILE,
                       "File containing settings for program options.", force)
    make_settings_file(system_defaults, SYSTEM_PARAMETERS_FILE,
                       "File containing settings for system parameters.",
                       force)
    make_settings_file(graph_defaults, GRAPH_SETTINGS_FILE,
                       "File containing settings for plotting.", force)


system_defaults = {
    "other_parameters": {
        "n": 1.e5,
        "u_a": 0.,
        "u_b": "-u_a",
        "kappa_a": 8.1,
        "kappa_b": "kappa_a",
        "omega_0": 0.047,
        "phi": 0.,
        "allow_rotating_solution": False
    },
    "single_point": {
        "omega_a": 40.,
        "omega_b": "omega_a",
        "g_a": "0.3/sqrt(n)",
        "g_b": "g_a",
        "gamma": 0.,
        "g_a_prime": "g_single",
        "g_b_prime": "g_a_prime"
    },
    "omega_list": {
        "omega_list_length": 20,
        "omega_list_length_modes": 1000,
        "omega_list_start": 40.,
        "omega_list_end": "-omega_list_start",
        "omega_b_equals": "'omega_a'"},
    "omega_b_list": {
        "omega_b_list_length": 20,
        "omega_b_list_start": 40.,
        "omega_b_list_end": "-omega_list_start",
    },
    "g_list": {
        "g_list_start": 1.e-4,
        "g_list_end": "1/sqrt(n)",
        "g_list_length_axis": "omega_list_length",
        "g_list_length_modes": 1000,
        "g_list_plots": "[g_a]",
        "g_prime_equals": "'g'"
    },
    "gamma_list": {
        "gamma_list_start": 0.,
        "gamma_list_end": 1.,
        "gamma_list_length": "omega_list_length",
        "gamma_list_plots": "[gamma]"
    },
    "steady_state_finder": {
        "zero": 1e-10
    },
    "phase_boundary": {
        "real_only": False
    },
    "time_evolver": {
        "start_time": 0.,
        "end_time": 5.,
        "number_of_steps": 1.e3,
        "omega_points": "[(omega_a, omega_b)]"
    },
    "initial_varaibles": {
        "alpha": 0.,
        "beta": 0.,
        "spin_plus": 0.,
        "spin_z": "-0.5 * n"
    }
}

graph_defaults = {
    "figures": {
        "width": 4.8,
        "multipanel_columns": 1,
    },
    "time_evolver": {
        "plot_bloch_sphere": False,
        "bloch_axes": True,
        "plot_surface": True,
        "plot_against_time": False,
        "plot_phi": False,
        "use_separate_axes": False,
        "plot_real_imag": False,
        "use_log_scale": False,
        "show_grid": False,
        "legend_location": "'best'"
    },
    "colourmaps": {
        "x_axis_g_root_n": False,
        "x_axis_khz": False,
        "show_phase_boundary": False,
        "plot_real_part_of_boundary": False,
        "colourbar": False,
        "title": False
    },
    "modes": {
        "tools": False,
        "plot_separate": False,
        "plot_real": True,
        "draw": [5, 6, 7]
    }
}
program_defaults = {
    "general_settings": {"verbose_level": 0,
                         "debug": False,
                         "logging": False,
                         "log_level": 1,
                         "save_figures": False,
                         "show_figures": True,
                         "print_parameters": False,
                         "number_of_workers": cpu_count()},
    "space": {"point": False,
              "modes_g": False,
              "modes_omega": False,
              "omega_v_g": False,
              "omega_a_v_omega_b": False,
              "omega_v_gamma": False,
              "time": False},
    "regime": {
        "normal": False,
        "inverted": False,
        "sr_g_prime_equals_g_sra": False,
        "sr_g_prime_equals_g_srb": False,
        "sr_g_prime_equals_zero": False,
        "sr_g_prime_equals_i_gamma_g_sra": False,
        "sr_g_prime_equals_i_gamma_g_srb": False,
        "sr_g_prime_equals_i_gamma_g": False
    },
    "third_axis": {
        "g": False,
        "gamma": False
    },
    "file_names": {"overwrite": False,
                   "write_file_name": "'test'",
                   "number": 0,
                   "read_file_name": "write_file_name",
                   "graph_file_name": "read_file_name",
                   "log_file_name": "'log'"},
    "pre_run_data_files": {
        "coefficients_sra_file": "'coefficients_spin_z_sra'",
        "coefficients_srb_file": "'coefficients_spin_z_srb'",
        "coefficients_zero_g_prime_file":
            "'coefficients_spin_z_zero_g_prime'",
        "coefficients_stationary_file":
            "'coefficients_spin_z_stationary'"}}

regimes = [regime for regime in program_defaults['regime']]
spaces = [space for space in program_defaults['space']]
third_axes = [axis for axis in program_defaults['third_axis']]

if __name__ == "__main__":
    main()
