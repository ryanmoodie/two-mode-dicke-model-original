#!/usr/bin/env python
# coding=utf-8

from cmath import log

from configurer import system_parameters

p = system_parameters()


class result:
    def __init__(self, result_list):
        self.alpha, self.beta, self.spin_plus, self.spin_z = result_list
        self.alpha_bar = self.alpha.conjugate()
        self.beta_bar = self.beta.conjugate()

        self.spin_x = self.spin_plus.real
        self.spin_y = self.spin_plus.imag
        self.spin_minus = self.spin_plus.conjugate()

        self.xi = ((self.alpha_bar + self.beta) - 1.j * p.gamma * (
            self.alpha - self.beta_bar))

        self.phi = 1.j / 2 * log(self.spin_minus / self.spin_plus)

        self.equ1 = self.alpha + p.g_a * (
            self.spin_minus + 1.j * p.gamma * self.spin_plus) / (
                                     p.omega_a - 1.j * p.kappa_a / 2)

        self.equ2 = self.beta + p.g_a * (
            self.spin_plus + 1.j * p.gamma * self.spin_minus) / (
                                    p.omega_b - 1.j * p.kappa_a / 2)

        self.equ3 = 2 * p.g_a * self.xi * self.spin_z \
                    - p.omega_0 * self.spin_plus

        self.half_equ4 = self.xi * self.spin_minus

        self.equ4 = self.half_equ4 - self.half_equ4.conjugate()

        self.test1 = self.test(self.equ1)
        self.test2 = self.test(self.equ2)
        self.test3 = self.test(self.equ3)
        self.test4 = self.test(self.equ4)

        self.alpha_dot = -(1.j * (p.omega_a * self.alpha + p.g_a * (
            self.spin_minus + 1.j * p.gamma * self.spin_plus))
                           + p.kappa_a * self.alpha / 2)

        self.beta_dot = -(1.j * (p.omega_b * self.beta + p.g_a * (
            self.spin_plus + 1.j * p.gamma * self.spin_minus))
                          + p.kappa_a * self.beta / 2)

        self.spin_plus_dot = -1.j * (
            -p.omega_0 * self.spin_plus + 2 * p.g_a * self.xi * self.spin_z)

        self.spin_z_dot = 1.j * p.g_a * self.equ4

    @staticmethod
    def test(equ):
        return abs(equ) < p.zero


dynamics = result([(4.8389399806025484 + 46.097270341529537j),
                   (4.8729396825664564e-13 - 7.3051930258828328e-13j),
                   (-35148.107464367851 + 35148.107464367946j),
                   (-5405.3263888888232 + 0j)])

stability = result([(-0 - 0j),
                    (-10.71734255488705 - 25.307338301876143j),
                    (34944.690945244816 + 34944.690945244816j),
                    -7598.496527777777])

for kind in [dynamics, stability]:
    print(getattr(kind, "equ4"))

# --------------------------- sympy experiments ----------------------------- #

# from numpy import arccos, pi
# from cmath import exp

# def phi_g_prime_equals_i_gamma_g(omega_a, omega_b):
#     return 0.5 * arccos(
#         (-(1 - p.gamma ** 2) * p.kappa_a * (omega_a + omega_b)) /
#         (2 * p.gamma * (omega_a * omega_b - p.kappa_a ** 2)))
#

# kappa = p.kappa_a
# kappa_prime = 0.5 * kappa
# g = p.g_a
# omega_0 = p.omega_0
# gamma = p.gamma
# phi = pi / 2
# omega_a = 5
# omega_b = 10

# from sympy import symbols, simplify, Poly, I, sqrt, Eq, solve, pi
# from sympy.functions import exp, conjugate
#
# omega_0, omega_a, omega_b, spin_z, kappa_prime, kappa, g, spin_plus, \
# phi, gamma, alpha, beta, spin_total = symbols(
#     'omega_0, omega_a, omega_b, '
#     'spin_z, kappa_prime, kappa, '
#     'g, spin_plus, phi, gamma,'
#     'alpha, beta, spin_total')
#
# kappa_prime = kappa / 2

# spin_z = Eq(
#     -omega_0 / (2 * g ** 2) * \
#     ((((1 + gamma ** 2) * omega_a - (1 - gamma ** 2) * 1.j * kappa_prime -
#        2 * 1.j * gamma * omega_a * exp(-1.j * 2 * phi))
#       / (omega_a ** 2 + kappa_prime ** 2)) +
#      (((1 + gamma ** 2) * omega_b + (1 - gamma ** 2) * 1.j * kappa_prime +
#        2 * 1.j * gamma * omega_b * exp(- 1.j * 2 * phi))
#       / (omega_b ** 2 + kappa_prime ** 2))) ** (-1),
#     spin_z)

# eq1 = Eq(alpha,
#          - g * (conjugate(spin_plus) + I * gamma * spin_plus) /
#          (omega_a - I * kappa_prime))
#
# eq2 = Eq(beta,
#          - g * (spin_plus + I * gamma * conjugate(spin_plus)) /
#          (omega_b - I * kappa_prime))
#
# eq3 = Eq(omega_0 * spin_plus,
#          2 * g * ((conjugate(alpha) + beta)
#                   - I * gamma * (alpha - conjugate(beta))) * spin_z)
#
# eq4 = Eq(((conjugate(alpha) + beta)
#           - I * gamma * (alpha - conjugate(beta))),
#          ((alpha + conjugate(beta))
#           + I * gamma * (conjugate(alpha) - beta)))
#
# eq5 = Eq(spin_total**2, spin_z ** 2 + spin_plus * conjugate(spin_plus))
#
# ans = solve([eq1, eq2, eq3, eq4, eq5], alpha, beta, spin_plus, spin_z)
#
# dic = [('g', p.g_a), ('omega_0', p.omega_0), ('kappa', p.kappa_a),
#        ('omega_a', p.omega_a), ('omega_b', p.omega_b), ('gamma', p.gamma)]

# print(ans)

# print([x.subs(dic) for x in ans])

# print(solve(equ, phi))

# class params:
#     def __init__(self, s, p):
#         self.s = s
#         self.p = p
#         self.omega_a = p.omega_a
#         self.omega_b = p.omega_b
#         self.phi = p.phi
#         self.g_a = p.g_a
#         self.g_b = p.g_b
#         self.g_a_p = p.g_a_p
#         self.g_b_p = p.g_b_p
#         self.u_a = p.u_a
#         self.u_b = p.u_b
#         self.gamma = p.gamma
#         self.coefficients = ""
#         self.normal = False
