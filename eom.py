#!/usr/bin/env python
# coding=utf-8

"""
eom.py
Module containing function to generate the equations of motion.
"""

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "21 Mar 2017"


def equations_of_motion(t, variables, parameters):
    alpha, beta, spin_plus, spin_z = variables
    omega_a, omega_b, omega_0, g_a, g_b, g_a_prime, g_b_prime, kappa_a, \
    kappa_b, u_a, u_b = parameters

    def alpha_dot():
        return - (1j * ((omega_a + u_a * spin_z) * alpha
                        + g_a * spin_plus.conjugate()
                        + g_a_prime * spin_plus)
                  + 0.5 * kappa_a * alpha)

    def beta_dot():
        return - (1j * ((omega_b + u_b * spin_z) * beta
                        + g_b * spin_plus
                        + g_b_prime * spin_plus.conjugate())
                  + 0.5 * kappa_b * beta)

    def J_plus_dot():
        return 1j * (spin_plus * (omega_0
                                  + u_a * alpha.conjugate() * alpha
                                  + u_b * beta.conjugate() * beta)
                     - 2 * (g_a * alpha.conjugate()
                            + g_a_prime.conjugate() * alpha
                            + g_b.conjugate() * beta
                            + g_b_prime * beta.conjugate()) * spin_z)

    def J_z_dot():
        return 1j * (
            g_a * alpha.conjugate() * spin_plus.conjugate()
            - g_a.conjugate() * alpha * spin_plus
            - g_a_prime * alpha.conjugate() * spin_plus
            + g_a_prime.conjugate() * alpha * spin_plus.conjugate()
            - g_b * beta.conjugate() * spin_plus
            + g_b.conjugate() * beta * spin_plus.conjugate()
            + g_b_prime * beta.conjugate() * spin_plus.conjugate()
            - g_b_prime.conjugate() * beta * spin_plus)

    return [alpha_dot(), beta_dot(), J_plus_dot(), J_z_dot()]


if __name__ == "__main__":
    exit()
