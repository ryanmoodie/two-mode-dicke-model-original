#!/usr/bin/env python
# coding=utf-8

"""
phase_finder_regimes.py
Module to define functions which use functions from phase_finder_spaces.py
module and have parameters filled appropriately for each option in main.py.
"""

from sys import exit

import phase_finder_spaces
import spin
from auxiliary import read_file_raw

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "31 Mar 2017"


def normal_point(s, p):
    return phase_finder_spaces.point(
        s, p, spin_z=spin.normal, normal=True, phi=lambda *params: p.phi)


def inverted_point(s, p):
    return phase_finder_spaces.point(
        s, p, spin_z=spin.inverted, normal=True, phi=lambda *params: p.phi)


def sr_g_prime_equals_g_sra_point(s, p):
    coefficients = read_file_raw(s, s.coefficients_sra)
    return phase_finder_spaces.point(
        s, p, spin_z=spin.numeric_spin, coefficients=coefficients,
        phi=lambda *params: p.phi)


def sr_g_prime_equals_g_srb_point(s, p):
    coefficients = read_file_raw(s, s.coefficients_srb)
    return phase_finder_spaces.point(
        s, p, spin_z=spin.numeric_spin, coefficients=coefficients,
        phi=lambda *params: p.phi)


def sr_g_prime_equals_zero_point(s, p):
    coefficients = read_file_raw(s, s.coefficients_zero_g_prime)
    return phase_finder_spaces.point(
        s, p, spin_z=spin.numeric_spin, coefficients=coefficients,
        phi=lambda *params: p.phi)


def sr_g_prime_equals_i_gamma_g_sra_point(s, p):
    return phase_finder_spaces.point(
        s, p, spin_z=spin.z_g_prime_equals_i_gamma_g,
        phi=spin.phi_g_prime_equals_i_gamma_g_sra)


def sr_g_prime_equals_i_gamma_g_point(s, p):
    return phase_finder_spaces.point(
        s, p, spin_z=spin.z_g_prime_equals_i_gamma_g,
        phi=spin.phi_g_prime_equals_i_gamma_g)


def sr_g_prime_equals_i_gamma_g_srb_point(s, p):
    return phase_finder_spaces.point(
        s, p, spin_z=spin.z_g_prime_equals_i_gamma_g,
        phi=spin.phi_g_prime_equals_i_gamma_g)


def inverted_omega_v_g_l_gamma(s, p):
    return [phase_finder_spaces.solve_state_stability_omega_v_g(
        s, p, spin_z=spin.inverted, phi=lambda *params: p.phi,
        omega_b=lambda omega_a: eval(p.omega_b_equals),
        gamma=gamma, g_p=lambda g, gamma: eval(p.g_prime_equals), normal=True)
        for gamma in p.gamma_list_plots]


def normal_omega_v_g_l_gamma(s, p):
    return [phase_finder_spaces.solve_state_stability_omega_v_g(
        s, p, spin_z=spin.normal, phi=lambda *params: p.phi,
        omega_b=lambda omega_a: eval(p.omega_b_equals),
        gamma=gamma, g_p=lambda g, gamma: eval(p.g_prime_equals), normal=True)
        for gamma in p.gamma_list_plots]


def sr_g_prime_equals_g_sra_omega_v_g(s, p):
    coefficients = read_file_raw(s, s.coefficients_sra)
    return [phase_finder_spaces.solve_state_stability_omega_v_g(
        s, p, spin_z=spin.numeric_spin, phi=lambda *params: p.phi,
        omega_b=lambda omega_a: eval(p.omega_b_equals), g_p=lambda g: g,
        coefficients=coefficients)]


def sr_g_prime_equals_g_srb_omega_v_g(s, p):
    coefficients = read_file_raw(s, s.coefficients_srb)
    return [phase_finder_spaces.solve_state_stability_xyz_omega_v_g(
        s, p, spin_x=spin.x_g_prime_equals_g_srb,
        spin_y=spin.y_g_prime_equals_g_srb,
        spin_z=spin.numeric_spin,
        omega_b=lambda omega_a: eval(p.omega_b_equals), g_p=lambda g: g,
        coefficients=coefficients)]


def sr_g_prime_equals_zero_omega_v_g(s, p):
    coefficients = read_file_raw(s, s.coefficients_zero_g_prime)
    return [phase_finder_spaces.solve_state_stability_omega_v_g(
        s, p, spin_z=spin.numeric_spin, phi=lambda *params: p.phi,
        omega_b=lambda omega_a: eval(p.omega_b_equals), g_p=lambda g: 0,
        coefficients=coefficients)]


def sr_g_prime_equals_i_gamma_g_omega_v_g_l_gamma(s, p):
    sol = phase_finder_spaces.solve_state_stability_omega_v_g_advanced(
        s, p, spin_z=spin.z_g_prime_equals_i_gamma_g,
        omega_b=lambda omega_a: eval(p.omega_b_equals),
        g_p=lambda g: 1.j * p.gamma * g,
        phi=spin.phi_g_prime_equals_i_gamma_g)

    sr = [[x == 1 for x in row] for row in sol]
    srb = [[x == 2 for x in row] for row in sol]
    sra = [[x == 3 for x in row] for row in sol]

    return [sr, srb, sra]


def normal_omega_v_gamma_l_g(s, p):
    return [(phase_finder_spaces.solve_state_stability_omega_v_gamma(
        s, p, spin_z=spin.normal, phi=lambda *params: p.phi,
        omega_b=lambda omega_a: eval(p.omega_b_equals),
        g=g, normal=True), g) for g in p.g_list_plots]


def inverted_omega_v_gamma_l_g(s, p):
    return [(phase_finder_spaces.solve_state_stability_omega_v_gamma(
        s, p, spin_z=spin.inverted, phi=lambda *params: p.phi,
        omega_b=lambda omega_a: eval(p.omega_b_equals),
        g=g, normal=True), g) for g in p.g_list_plots]


def sr_g_prime_equals_i_gamma_g_omega_v_gamma_l_g(s, p):
    results = []
    for g in p.g_list_plots:
        sol = phase_finder_spaces.solve_state_stability_omega_v_gamma_advanced(
            s, p, spin_z=spin.z_g_prime_equals_i_gamma_g,
            omega_b=lambda omega_a: eval(p.omega_b_equals),
            g=g, phi=spin.phi_g_prime_equals_i_gamma_g)

        sr = [[x == 1 for x in row] for row in sol]
        srb = [[x == 2 for x in row] for row in sol]
        sra = [[x == 3 for x in row] for row in sol]

        results.append((sr, g))
        results.append((srb, g))
        results.append((sra, g))

    return results


def normal_omega_a_v_omega_b_l_g(s, p):
    return [
        (phase_finder_spaces.solve_state_stability_omega_a_v_omega_b(
            s, p, spin_z=spin.normal, g=g,
            g_p=lambda g, gamma: eval(p.g_prime_equals),
            gamma=p.gamma, phi=lambda *params: p.phi, normal=True), g)
        for g in p.g_list_plots]


def inverted_omega_a_v_omega_b_l_g(s, p):
    return [
        (phase_finder_spaces.solve_state_stability_omega_a_v_omega_b(
            s, p, spin_z=spin.inverted, g=g,
            g_p=lambda g, gamma: eval(p.g_prime_equals),
            gamma=p.gamma, phi=lambda *params: p.phi, normal=True), g)
        for g in p.g_list_plots]


def normal_omega_a_v_omega_b_l_gamma(s, p):
    return [
        (phase_finder_spaces.solve_state_stability_omega_a_v_omega_b(
            s, p, spin_z=spin.normal, g=p.g_a,
            g_p=lambda g, gamma: eval(p.g_prime_equals),
            gamma=gamma, phi=lambda *params: p.phi, normal=True), gamma)
        for gamma in p.gamma_list_plots]


def inverted_omega_a_v_omega_b_l_gamma(s, p):
    return [
        (phase_finder_spaces.solve_state_stability_omega_a_v_omega_b(
            s, p, spin_z=spin.inverted, g=p.g_a, g_p=eval(p.g_prime_equals),
            gamma=gamma, phi=lambda *params: p.phi, normal=True), gamma)
        for gamma in p.gamma_list_plots]


def sr_g_prime_equals_g_sra_omega_a_v_omega_b(s, p):
    coefficients = read_file_raw(s, s.coefficients_sra)
    return [phase_finder_spaces.solve_state_stability_omega_a_v_omega_b(
        s, p, spin_z=spin.numeric_spin, g=p.g_a,
        g_p=p.g_a, coefficients=coefficients, phi=lambda *params: p.phi)]


def sr_g_prime_equals_g_srb_omega_a_v_omega_b(s, p):
    coefficients = read_file_raw(s, s.coefficients_srb)
    return [phase_finder_spaces.solve_state_stability_xyz_omega_a_v_omega_b(
        s, p, spin_x=spin.x_g_prime_equals_g_srb,
        spin_y=spin.y_g_prime_equals_g_srb, spin_z=spin.numeric_spin,
        g=p.g_a, g_p=p.g_a, coefficients=coefficients)]


def sr_g_prime_equals_zero_omega_a_v_omega_b(s, p):
    coefficients = read_file_raw(s, s.coefficients_zero_g_prime)
    return [phase_finder_spaces.solve_state_stability_omega_a_v_omega_b(
        s, p, spin_z=spin.numeric_spin, g=p.g_a, g_p=0,
        coefficients=coefficients, phi=lambda *params: p.phi)]


def sr_g_prime_equals_i_gamma_g_omega_a_v_omega_b_l_g(s, p):
    results = []
    for g in p.g_list_plots:
        sol = phase_finder_spaces. \
            solve_state_stability_omega_a_v_omega_b_advanced(
            s, p, spin_z=spin.z_g_prime_equals_i_gamma_g, g=g,
            g_p=1.j * p.gamma * g, phi=spin.phi_g_prime_equals_i_gamma_g)

        sr = [[x == 1 for x in row] for row in sol]
        srb = [[x == 2 for x in row] for row in sol]
        sra = [[x == 3 for x in row] for row in sol]

        results.append((sr, g))
        results.append((srb, g))
        results.append((sra, g))

    return results


def sr_g_prime_equals_i_gamma_g_omega_a_v_omega_b_l_gamma(s, p):
    results = []
    for gamma in p.gamma_list_plots:
        sol = phase_finder_spaces. \
            solve_state_stability_omega_a_v_omega_b_advanced(
            s, p, spin_z=spin.z_g_prime_equals_i_gamma_g, g=p.g_a,
            g_p=1.j * gamma * p.g_a, phi=spin.phi_g_prime_equals_i_gamma_g)

        sr = [[x == 1 for x in row] for row in sol]
        srb = [[x == 2 for x in row] for row in sol]
        sra = [[x == 3 for x in row] for row in sol]

        results.append((sr, gamma))
        results.append((srb, gamma))
        results.append((sra, gamma))

    return results


def inverted_modes_g(s, p):
    return [phase_finder_spaces.solve_modes_g(
        s, p, g_p=lambda g: eval(p.g_prime_equals),
        spin_z_func=spin.inverted, normal=True,
        phi=lambda *params: p.phi)]


def normal_modes_g(s, p):
    return [phase_finder_spaces.solve_modes_g(
        s, p, g_p=lambda g: eval(p.g_prime_equals),
        spin_z_func=spin.normal, normal=True,
        phi=lambda *params: p.phi)]


def sr_g_prime_equals_i_gamma_g_sra_modes_g(s, p):
    return [phase_finder_spaces.solve_modes_g(
        s, p, g_p=lambda g: 1.j * p.gamma * g,
        spin_z_func=spin.z_g_prime_equals_i_gamma_g,
        phi=spin.phi_g_prime_equals_i_gamma_g_sra)]


def sr_g_prime_equals_i_gamma_g_srb_modes_g(s, p):
    return [phase_finder_spaces.solve_modes_g(
        s, p, g_p=lambda g: 1.j * p.gamma * g,
        spin_z_func=spin.z_g_prime_equals_i_gamma_g,
        phi=spin.phi_g_prime_equals_i_gamma_g)]


def sr_g_prime_equals_i_gamma_g_modes_g(s, p):
    return [phase_finder_spaces.solve_modes_g(
        s, p, g_p=lambda g: 1.j * p.gamma * g,
        spin_z_func=spin.z_g_prime_equals_i_gamma_g,
        phi=spin.phi_g_prime_equals_i_gamma_g)]


def sr_g_prime_equals_g_sra_modes_g(s, p):
    return [phase_finder_spaces.solve_modes_g(
        s, p, g_p=lambda g: 1.j * p.gamma * g,
        spin_z_func=spin.numeric_spin, phi=lambda *params: p.phi,
        coefficients=read_file_raw(s, s.coefficients_sra))]


def inverted_modes_omega(s, p):
    return [phase_finder_spaces.solve_modes_omega(
        s, p, omega_b=lambda omega_a: eval(p.omega_b_equals),
        spin_z_func=spin.inverted, phi=lambda *params: p.phi,
        normal=True)]


def normal_modes_omega(s, p):
    return [phase_finder_spaces.solve_modes_omega(
        s, p, omega_b=lambda omega_a: eval(p.omega_b_equals),
        spin_z_func=spin.normal, phi=lambda *params: p.phi,
        normal=True)]


def sr_g_prime_equals_i_gamma_g_sra_modes_omega(s, p):
    return [phase_finder_spaces.solve_modes_omega(
        s, p, omega_b=lambda omega_a: eval(p.omega_b_equals),
        phi=spin.phi_g_prime_equals_i_gamma_g_sra,
        spin_z_func=spin.z_g_prime_equals_i_gamma_g)]


def sr_g_prime_equals_i_gamma_g_srb_modes_omega(s, p):
    return [phase_finder_spaces.solve_modes_omega(
        s, p, omega_b=lambda omega_a: eval(p.omega_b_equals),
        phi=spin.phi_g_prime_equals_i_gamma_g,
        spin_z_func=spin.z_g_prime_equals_i_gamma_g)]


def sr_g_prime_equals_i_gamma_g_modes_omega(s, p):
    return [phase_finder_spaces.solve_modes_omega(
        s, p, omega_b=lambda omega_a: eval(p.omega_b_equals),
        phi=spin.phi_g_prime_equals_i_gamma_g,
        spin_z_func=spin.z_g_prime_equals_i_gamma_g)]


if __name__ == "__main__":
    exit()
