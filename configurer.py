#!/usr/bin/env python
# coding=utf-8

"""
configurer.py
Module to read values from settings (.cfg) file and calculate values to 
populate settings classes.
"""

from configparser import ConfigParser
# math may not be used explicitly, but required for eval() !
from math import sqrt, pi, exp
from sys import exit

from numpy import linspace

import defaults

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "01 Feb 2017"


def get_file_path_start(folder, pre_name):
    return f"{folder}/{pre_name}-"


def get_file_path_end(number, suffix):
    return f"-{number}.{suffix}"


class Settings:
    def __init__(self, default_dict, settings_file):
        user = ConfigParser()
        default = ConfigParser()
        user.read(settings_file)
        default.read_dict(default_dict)

        for section in user.sections():
            for key, value in user[section].items():
                setattr(self, key, eval(
                    default[section][key] if value == "" else value,
                    None, vars(self)))


def system_parameters(extra=''):
    def list_fixed(list_start, list_end, list_number):
        list_step = abs(list_start - list_end) / (list_number - 1)

        if list_end < list_start:
            list_end_fixed = list_end - list_step
        elif list_end > list_start:
            list_end_fixed = list_end + list_step

        list_number_fixed = list_number + 1
        return linspace(list_start, list_end_fixed, num=list_number_fixed)

    p = Settings(defaults.system_defaults,
                 defaults.get_settings_path(
                     f'{defaults.SYSTEM_PARAMETERS_FILE}{extra}'))

    p.g_list_end += \
        abs(p.g_list_start - p.g_list_end) / p.g_list_length_axis
    p.g_list_length_axis += 1

    p.spin_total = 0.5 * p.n

    p.omega_list = list_fixed(
        p.omega_list_start, p.omega_list_end, p.omega_list_length)

    p.omega_b_list = list_fixed(
        p.omega_b_list_start, p.omega_b_list_end, p.omega_b_list_length)

    p.omega_list_modes = linspace(
        p.omega_list_start, p.omega_list_end,
        num=p.omega_list_length_modes)

    p.g_list = list_fixed(
        p.g_list_start, p.g_list_end, p.g_list_length_axis)

    p.g_list_modes = linspace(
        p.g_list_start, p.g_list_end, num=p.g_list_length_modes)

    p.gamma_list = list_fixed(
        p.gamma_list_start, p.gamma_list_end, p.gamma_list_length)

    p.time_step = (p.end_time - p.start_time) / p.number_of_steps

    p.initial_variables = [p.alpha, p.beta, p.spin_plus, p.spin_z]

    return p


def graph_settings():
    g = Settings(defaults.graph_defaults,
                 defaults.get_settings_path(defaults.GRAPH_SETTINGS_FILE))
    return g


def program_configuration(extra=''):
    s = Settings(defaults.program_defaults, defaults.get_settings_path(
        f'{defaults.PROGRAM_CONFIGURATION_FILE}{extra}'))

    data_file_suffix = "pkl"
    image_file_suffix = "pdf"
    log_file_suffix = "log"

    graph_folder = "graphs"
    data_folder = "simulation_data"
    log_folder = "log"
    pre_run_data_folder = "pre_run_data"

    s.write_file_start = \
        get_file_path_start(data_folder, s.write_file_name)
    s.write_file_end = get_file_path_end(s.number, data_file_suffix)

    s.graph_file_start = \
        get_file_path_start(graph_folder, s.graph_file_name)
    s.graph_file_end = get_file_path_end(s.number, image_file_suffix)

    s.read_file_start = \
        get_file_path_start(data_folder, s.read_file_name)
    s.read_file_end = get_file_path_end(s.number, data_file_suffix)

    s.log_file_start = \
        get_file_path_start(log_folder, s.log_file_name)
    s.log_file_end = \
        get_file_path_end(s.number, log_file_suffix)

    s.coefficients_sra = f"{pre_run_data_folder}/" \
                         f"{s.coefficients_sra_file}.{data_file_suffix}"
    s.coefficients_srb = f"{pre_run_data_folder}/" \
                         f"{s.coefficients_srb_file}.{data_file_suffix}"
    s.coefficients_zero_g_prime = f"{pre_run_data_folder}/" \
                                  f"{s.coefficients_zero_g_prime_file}." \
                                  f"{data_file_suffix}"
    s.coefficients_stationary = f"{pre_run_data_folder}/" \
                                f"{s.coefficients_stationary_file}." \
                                f"{data_file_suffix}"

    return s


if __name__ == "__main__":
    exit()
