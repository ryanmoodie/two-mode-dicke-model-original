#!/usr/bin/env python
# coding=utf-8

"""
matrices.py
Module containing functions to generate the perturbation matrices.
"""

from cmath import sqrt as csqrt

from numpy import array, exp

from auxiliary import plog

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "21 Mar 2017"


def perturbation_matrix_xyz(s, p, spin_x_0, spin_y_0, spin_z_0, omega_a,
                            omega_b, g_a, g_b, g_a_p, g_b_p, u_a, u_b,
                            **params):
    kappa_a = p.kappa_a
    kappa_b = p.kappa_b
    omega_0 = p.omega_0

    G_a_plus = g_a + g_a_p
    G_a_minus = g_a - g_a_p
    G_b_plus = g_b + g_b_p
    G_b_minus = g_b - g_b_p

    omega_a_tilde = omega_a + u_a * spin_z_0
    omega_b_tilde = omega_b + u_b * spin_z_0

    alpha_0 = - (G_a_plus * spin_x_0 - 1.j * G_a_minus * spin_y_0) / (
        omega_a_tilde - 0.5j * kappa_a)

    beta_0 = - (G_b_plus * spin_x_0 + 1.j * G_b_minus * spin_y_0) / (
        omega_b_tilde - 0.5j * kappa_b)

    alpha_0_mod_sq = alpha_0 * alpha_0.conjugate()
    beta_0_mod_sq = beta_0 * beta_0.conjugate()

    term = 1.j * (omega_0 + u_a * alpha_0_mod_sq + u_b * beta_0_mod_sq)

    matrix = array(
        [
            [
                omega_a_tilde - 0.5j * kappa_a,
                0,
                0,
                0,
                G_a_plus,
                - 1.j * G_a_minus,
                u_a * alpha_0
            ],
            [
                0,
                - omega_a_tilde - 0.5j * kappa_a,
                0,
                0,
                - G_a_plus.conjugate(),
                - 1.j * G_a_minus.conjugate(),
                - u_a * alpha_0.conjugate()
            ],
            [
                0,
                0,
                omega_b_tilde - 0.5j * kappa_b,
                0,
                G_b_plus,
                1.j * G_b_minus,
                u_b * beta_0
            ],
            [
                0,
                0,
                0,
                - omega_b_tilde - 0.5j * kappa_b,
                - G_b_plus.conjugate(),
                1.j * G_b_minus.conjugate(),
                - u_b * beta_0.conjugate()
            ],
            [
                - 1.j * u_a * spin_y_0 * alpha_0.conjugate(),
                - 1.j * u_a * spin_y_0 * alpha_0,
                - 1.j * u_b * spin_y_0 * beta_0.conjugate(),
                - 1.j * u_b * spin_y_0 * beta_0,
                0,
                - term,
                0
            ],
            [
                (- 1.j * (u_a * spin_x_0 * alpha_0.conjugate()
                          + 2 * spin_z_0 * g_a_p.conjugate())),
                (- 1.j * (u_a * spin_x_0 * alpha_0
                          + 2 * spin_z_0 * g_a)),
                (- 1.j * (u_b * spin_x_0 * beta_0.conjugate()
                          + 2 * spin_z_0 * g_b.conjugate())),
                (- 1.j * (u_b * spin_x_0 * beta_0
                          + 2 * spin_z_0 * g_b_p)),
                term,
                0,
                (- 2.j * (g_a * alpha_0.conjugate()
                          + g_a_p.conjugate() * alpha_0
                          + g_b.conjugate() * beta_0
                          + g_b_p * beta_0.conjugate()))
            ],
            [
                (1.j * G_a_plus.conjugate() * spin_y_0
                 + G_a_minus.conjugate() * spin_x_0),
                (1.j * G_a_plus * spin_y_0
                 - G_a_minus * spin_x_0),
                (1.j * G_b_plus.conjugate() * spin_y_0
                 - G_b_minus.conjugate() * spin_x_0),
                (1.j * G_b_plus * spin_y_0
                 + G_b_minus * spin_x_0),
                (G_a_minus.conjugate() * alpha_0
                 - G_a_minus * alpha_0.conjugate()
                 + G_b_minus * beta_0.conjugate()
                 - G_b_minus.conjugate() * beta_0),
                (1.j * (G_a_plus.conjugate() * alpha_0
                        + G_a_plus * alpha_0.conjugate()
                        + G_b_plus * beta_0.conjugate()
                        + G_b_plus.conjugate() * beta_0)),
                0
            ]
        ]
    )

    plog(s, f"Matrix:\n{matrix}", 2)

    return matrix


def perturbation_matrix(s, p, spin_z_0, omega_a, omega_b, g_a, g_b, g_a_p,
                        g_b_p, u_a, u_b, **params):
    kappa_a = p.kappa_a
    kappa_b = p.kappa_b
    omega_0 = p.omega_0

    if (not params["gamma"]) and p.allow_rotating_solution:
        mu = (omega_a + u_a * spin_z_0 - omega_b - u_b * spin_z_0) / 2
        omega_0 = omega_0 - mu
        omega_a = omega_a - mu
        omega_b = omega_b + mu

    spin_plus_0 = params["spin_plus_0"]
    alpha_0 = params["alpha_0"]
    beta_0 = params["beta_0"]

    omega_a_tilde = omega_a + u_a * spin_z_0
    omega_b_tilde = omega_b + u_b * spin_z_0

    gamma_term = (g_a_p.conjugate() * alpha_0
                  + g_a * alpha_0.conjugate()
                  + g_b.conjugate() * beta_0
                  + g_b_p * beta_0.conjugate())

    omega_0_tilde = (omega_0 + u_b * beta_0 * beta_0.conjugate()
                     + u_a * alpha_0 * alpha_0.conjugate())

    matrix = array(
        [
            [
                (omega_a_tilde - 0.5j * kappa_a),
                0.,
                0.,
                0.,
                g_a_p,
                g_a,
                (u_a * alpha_0)
            ],
            [
                0.,
                (- (omega_a_tilde + 0.5j * kappa_a)),
                0.,
                0.,
                (- g_a.conjugate()),
                (- g_a_p.conjugate()),
                (- u_a * alpha_0.conjugate())
            ],
            [
                0.,
                0.,
                (omega_b_tilde - 0.5j * kappa_b),
                0.,
                g_b,
                g_b_p,
                (u_b * beta_0)
            ],
            [
                0.,
                0.,
                0.,
                (- (omega_b_tilde + 0.5j * kappa_b)),
                (- g_b_p.conjugate()),
                (- g_b.conjugate()),
                (- u_b * beta_0.conjugate())
            ],
            [
                (2. * g_a_p.conjugate() * spin_z_0
                 - u_a * spin_plus_0 * alpha_0.conjugate()),
                (2. * g_a * spin_z_0 - u_a * spin_plus_0 * alpha_0),
                (2. * g_b.conjugate() * spin_z_0
                 - u_b * spin_plus_0 * beta_0.conjugate()),
                (2. * g_b_p * spin_z_0 - u_b * spin_plus_0 * beta_0),
                -omega_0_tilde,
                0.,
                2. * gamma_term
            ],
            [
                (- 2. * g_a.conjugate() * spin_z_0
                 + u_a * spin_plus_0.conjugate() * alpha_0.conjugate()),
                (- 2. * g_a_p * spin_z_0
                 + u_a * spin_plus_0.conjugate() * alpha_0),
                (- 2. * g_b_p.conjugate() * spin_z_0
                 + u_b * spin_plus_0.conjugate() * beta_0.conjugate()),
                (- 2. * g_b * spin_z_0
                 + u_b * spin_plus_0.conjugate() * beta_0),
                0.,
                omega_0_tilde,
                (- 2. * gamma_term.conjugate())
            ],
            [
                (- g_a_p.conjugate() * spin_plus_0.conjugate()
                 + g_a.conjugate() * spin_plus_0),
                (- g_a * spin_plus_0.conjugate()
                 + g_a_p * spin_plus_0),
                (- g_b.conjugate() * spin_plus_0.conjugate()
                 + g_b_p.conjugate() * spin_plus_0),
                (- g_b_p * spin_plus_0.conjugate()
                 + g_b * spin_plus_0),
                gamma_term.conjugate(),
                -gamma_term,
                0.
            ]
        ]
    )

    plog(s, f"Matrix:\n{matrix}", 2)

    return matrix


if __name__ == "__main__":
    exit()
